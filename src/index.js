import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import './index.css';
import 'semantic-ui-css/semantic.min.css';

/** Ponto de entrada da aplicação, onde o componente principal é renderizado. */
ReactDOM.render(
  <App />,
  document.getElementById('root')
);

/** TODO: migrar esse controle para o component quando for liberado o component Sidebar(stardust) */
$('.ui.sidebar').sidebar({
	context: $('.bottom.segment')
});
