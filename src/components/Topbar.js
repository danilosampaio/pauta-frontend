import React, { Component } from 'react';
import { Menu, Icon, Label } from 'stardust';
import UserProfile from './UserProfile';

/**
Barra superior da aplicação, onde se localiza o menu principal, e ações de perfil do usuário.
*/
class Topbar extends Component {

  constructor(props) {
    super(props);

    this.selectModule = this.selectModule.bind(this);
    this.closeModule = this.closeModule.bind(this);
  }

  closeModule(el, module) {
    this.props.closeModule(module.id);
  }

  selectModule(el, module) {
    //TODO
    if (el.nativeEvent.target.getAttribute('class') === 'delete icon') {
      return;
    }
    this.props.selectModule(module.id);
  }

  render() {
    return (
      <Menu 
        borderless={true}
        attached='top'>

        <Menu.Item
          name='menu'
          onClick={this.props.toggleMenu} >

          <Icon name='list' /> Menu        
        </Menu.Item>

        <Menu.Item
          name='openModules' >

          {Object.keys(this.props.openModules).map((k) =>
            <Label 
              key={k} 
              content={this.props.openModules[k].label}
              circular={true}
              removable={true}
              id={this.props.openModules[k]}
              color={this.props.openModules[k].active ? 'teal' : null}
              onClick={this.selectModule}
              onRemove={this.closeModule} >
            </Label>
          )}
        </Menu.Item>

        <Menu.Item
          name='profile'
          position='right'
          link={false} >
          
          <UserProfile 
            logout={this.props.logout} />            
        </Menu.Item>
      </Menu>
    )
  }
}

export default Topbar;
