import React from 'react'
import { Dropdown, Icon } from 'stardust'

const trigger = (
  <Icon 
    name='small user'
    circular />
)

/** Menu dropdown com as ações do perfil do usuário. */
const UserProfile = (props) => (
  <Dropdown trigger={trigger} pointing='top right' icon={null}>
    <Dropdown.Menu>
      <Dropdown.Item 
        text='Configurações' 
        icon='settings' />
      <Dropdown.Item 
        text='Sair' 
        icon='sign out'
        onClick={props.logout} />
    </Dropdown.Menu>
  </Dropdown>
)

export default UserProfile;
