import React, { Component } from 'react';
import { Menu } from 'stardust';

/**
Menu principal da aplicação. É exibido do lado esquerdo(sidebar) ao clicar no menu da barra superior.
*/
class LeftMenu extends Component {
  
  constructor(props) {
    super(props);

    this.modules = [
      {
        header: 'Projetos',
        subitems: [
          {
            name: "dashboard",
            label: "Dashboard"
          },
          {
            name: "gantt",
            label: "Cronograma - Gantt"
          },
          {
            name: "multaDiaria",
            label: "Multa diária"
          },
          {
            name: "ape",
            label: "Atraso por Entrega - APE"
          },
          {
            name: "pmas",
            label: "Prazo Máximo para Aceite do Serviço - PMAS"
          },
          {
            name: "pmcos",
            label: "Prazo Máximo para Conclusão da Ordem de Serviço - PMCOS"
          }
        ]
      },
      {
        header: 'Suporte Operacional',
        subitems: [
          {
            name: "incidentes",
            label: "Incidentes"
          },
          {
            name: "atividades",
            label: "Atividades"
          }
        ]
      },
      {
        header: 'Contrato BNB',
        subitems: [
          {
            name: "accrual",
            label: "Accrual"
          },
          {
            name: "pa",
            label: "Processos Administrativos"
          },
          {
            name: "baseDeConhecimento",
            label: "Base de Conhecimento"
          },
          {
            name: "time",
            label: "Time"
          }
        ]
      }
    ];

    this.handleItemClick = this.handleItemClick.bind(this);
  }

  findModule(name) {
    for (var i = 0; i < this.modules.length; i++) {
      var group = this.modules[i];
      for (var j = 0; j < group.subitems.length; j++) {
        var s = group.subitems[j];
        if (s.name === name) {
          return s;
        }
      };
    };
  }


  handleItemClick(el, item){
    this.props.openModule(this.findModule(item.name));
  }
  
  render() {
    return (      
      <Menu
        inverted={true}
        vertical={true}
        className='sidebar'>
        {this.modules.map((g,i) =>
          <MenuGroup key={i} group={g} handleItemClick={this.handleItemClick}/>
        )}
      </Menu>
    )
  }
}

class MenuGroup extends Component {
  render() {
    return (
      <Menu.Item>
        <Menu.Header>{this.props.group.header}</Menu.Header>
        <Menu.Menu>
          {this.props.group.subitems.map((s,i) =>
            <Menu.Item 
              index={i}
              key={i} 
              name={s.name}
              onClick={this.props.handleItemClick}>
              {s.label}
            </Menu.Item>
          )}
        </Menu.Menu>
      </Menu.Item>
    )
  }
}

export default LeftMenu;