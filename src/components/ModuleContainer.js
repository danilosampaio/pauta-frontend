import React, { Component } from 'react';
import { Segment } from 'stardust';

class ModuleContainer extends Component {
  
  render() {
    return (
      <div className="pusher">
        <div id="moduleContainer">
          {this.props.activeModule ?  
            (<Segment>
            {this.props.activeModule.label}
          </Segment>) : ''}
        </div>
      </div>
    )
  }
}

export default ModuleContainer;