import React, { Component } from 'react';
import Topbar from './Topbar';
import LeftMenu from './LeftMenu';
import ModuleContainer from './ModuleContainer';

/** 
Componente principal da aplicação, que compôe todas as outras partes, 
e centraliza o controle do estado da aplicação. 
*/
class App extends Component {

  constructor(props) {
    super(props);

    /** Estado centralizado da aplicação */
    this.state = {
      /** armazena os módulos que estão abertos na aplicação (para o usuário, as telas que estão abertas) */
      openModules: {},
      /** armazena o módulo ativo (para o usuário, a tela que está visível) */
      activeModule: null,
      /** armazena o módulo anteriormente ativo (caso o módulo atualmente ativo seja fechado, a aplicação tenta exibir o anterior. */
      previousModule: null,
      /** usuário logado */
      user: null
    }

    /** método do escopo do componente */
    this.selectModule = this.selectModule.bind(this);
    this.closeModule = this.closeModule.bind(this);
    this.openModule = this.openModule.bind(this);
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  /** Exibe o menu principal da aplicação(sidebar) */
  toggleMenu() {
    $('.ui.sidebar').sidebar('toggle');
  }

  /** TODO: Exibe dropdown com as opções do perfil do usuário (logout, configurações) */
  openProfile(){
    console.log('user profile');
  }

  /** abre o módulo informado, ou ativa caso o mesmo já esteja aberto. */
  selectModule(module) {
    let openModules = {...this.state.openModules};
    for (let m in openModules) {
      openModules[m].active = false;
    }
    module.active = true;
    openModules[module.name] = module;

    this.setState({
      activeModule: module,
      openModules: openModules,
      previousModule: this.state.activeModule
    });
  }

  //TODO ainda não está setando o módulo ativo corretamente.
  closeModule(module) {
    let openModules = {...this.state.openModules};
    delete openModules[module.name];
    let activeModule = null;
    if (this.state.previousModule) {
      activeModule = {...this.state.previousModule};
      activeModule.active = true;
    }
    this.setState({
      activeModule: activeModule,
      openModules: openModules,
      previousModule: null      
    })
  }

  /** abre o módulo informado, ou ativa caso o mesmo já esteja aberto, abre/fecha o menu principal. */
  openModule(module) {
    this.selectModule(module);

    this.toggleMenu();
  }

  /** TODO: Faz logout da aplicação. */
  logout() {
    console.log('logout')
  }

  
  render() {
    return (
      <div>
        <Topbar 
          toggleMenu={this.toggleMenu}
          selectModule={this.selectModule}
          openModule={this.openModule}
          closeModule={this.closeModule}
          openProfile={this.openProfile}
          openModules={this.state.openModules} 
          logout={this.logout} />
        
        <div className="ui bottom attached segment pushable">
          <LeftMenu 
            toggleMenu={this.toggleMenu}
            openModule={this.openModule} 
            ref='_leftMenu' />
          
          <ModuleContainer 
            openModules={this.state.openModules} 
            activeModule={this.state.activeModule}
            ref='_moduleContainer' />
        </div>
      </div>
    )
  }
}

export default App;